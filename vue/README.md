# vue-node-docker-boilerplate

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### build for production and view the bundle analyzer report
```
npm run build --report
```

### Run your unit tests ([Checkout Vue test Utils](https://vue-test-utils.vuejs.org))
```
npm run test:unit
```

### Run your end-to-end tests ([Checkout Cypress](https://www.cypress.io))
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
