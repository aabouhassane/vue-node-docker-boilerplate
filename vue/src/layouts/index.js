/*
 * Leyton 4 Me
 * Copyright (c) 2020 Leyton Group - All Rights Reserved
 * contact@leyton.me
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import Vue from 'vue';
import upperFirst from 'lodash/upperFirst';
import camelCase from 'lodash/camelCase';

/* ======================
 * Global Components
 * ======================
 *
 * The following block will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./layouts/noSidebar.vue -> <no-sidebar-layout></no-sidebar-layout>
 */
const layouts1 = require.context('./wrappers', false, /\.vue$/i);
layouts1.keys().map((fileIndexKey) => {
  const componentConfig = layouts1(fileIndexKey);
  const componentName = upperFirst(
      camelCase(
          fileIndexKey
              .replace(/^\.\//, '')
              .replace(/\.\w+$/, '')) // .replace(/\w+\//, '')
  ).concat('Layout');

  Vue.component(componentName, componentConfig.default || componentConfig);
});
