export default [
  {
    path: '/',
    name: 'Dashboard',
    view: 'Dashboard',
    meta: {}
  },
  {
    path: '/notfound',
    name: 'NotFound',
    view: 'NotFound',
    meta: {}
  },
]
