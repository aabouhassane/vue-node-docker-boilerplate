import Vue from 'vue';
import upperFirst from 'lodash/upperFirst';
import camelCase from 'lodash/camelCase';


/** Load all vue files in this folder, recursively */
const requireComponent = require.context(
    './', true, /\.vue$/
);

requireComponent
    .keys()
    .filter((file) => !file.toLowerCase().includes('/global/'))
    .forEach((fileName) => {
      // Load component
      const componentConfig = requireComponent(fileName);

      // Get component name
      const componentName = upperFirst(
          camelCase(fileName.replace(/^\.\//, '').replace(/\.\w+$/, ''))
      );

      // Register component
      Vue.component(`Leyton${componentName}`,
          componentConfig.default || componentConfig);
    });
