/* ============
 * Main File
 * ============
 *
 * Will initialize the application.
 */
import Vue from 'vue';

/* ============
 * Plugins
 * ============
 *
 * Import and bootstrap the plugins.
 */
import '@/plugins/vuex';
import '@/plugins/axios';
import { router } from './plugins/vue-router';
import '@/plugins/vuex-router-sync';
import vuetify from '@/plugins/vuetify';

/* ====================
 * External Components
 * ====================
 *
 * Import and bootstrap external components
 * Note : imports of unused components are removed. See : fa64d59a7dfbdd0d5bc1b7d1812a3faac4a23412
 */

/* ============
 * Main App
 * ============
 *
 * we import the main application files.
 */
import App from '@/App.vue';
import store from '@/store';

/* ===================
 * Mixins & Filters
 * ===================
 *
 * Import reusable mthods and mixins
 */
import generalMixin from '@/mixins/generalMixin';

Vue.mixin(generalMixin);

/* ======================
 * Global Components
 * ======================
 */
import '@/components';
import '@/layouts';

/*
 * disable mode production
 */
Vue.config.productionTip = false

new Vue({
  vuetify,
  components: {},
  data: {},
  watch: {},
  created() {
    if (this.isMobile) {
      document.body.classList.add('is-mobile');
    }
  },
  methods: {},
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
