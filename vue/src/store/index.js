/* ============
 * Vuex Store
 * ============
 *
 * The store of the application.
 *
 * http://vuex.vuejs.org/en/index.html
 */

import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';

// Modules
import modules from './modules';

const debug = process.env.VUE_APP_NODE_ENV !== 'production';

export default new Vuex.Store({
    state: {},
    getters: {},
    mutations: {},
    actions: {},
    modules,
    strict: debug,
    plugins: debug ? [createLogger()] : [],
});
