/*
 * Leyton 4 Me
 * Copyright (c) 2020 Leyton Group - All Rights Reserved
 * contact@leyton.me
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

/* ============
 * Vuex Store
 * ============
 *
 * Vuex is a state management pattern + library for Vue.js applications.
 * It serves as a centralized store for all the components in an application,
 * with rules ensuring that the state can only
 * be mutated in a predictable fashion.
 *
 * http://vuex.vuejs.org/en/index.html
 */

import Vue from 'vue';
import Vuex from 'vuex';

// Load vuex
Vue.use(Vuex);
