/* ============
 * Axios
 * ============
 *
 * Promise based HTTP client for the browser and node.js.
 * Because Vue Resource has been retired, Axios will now been used
 * to perform AJAX-requests.
 *
 * https://github.com/mzabriskie/axios
 */

import Vue from 'vue';
import Axios from 'axios';

Axios.defaults.baseURL = process.env.VUE_APP_BASE_LOCATION + '/api';
Axios.defaults.headers.common.Accept = 'application/json';
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

Axios.interceptors.response.use(
    (response) => {
      // Refresh token for example
      return response;
    },
    (error) => {
      if (error.response.status === 401) {
        // The user ain't authenticated : empty auth token and redirect to login page
      } else if (error.response.status === 403) {
          // Unauthorized
          // Vue.router.push('/');
      } else {
        // Show Error
      }

      return Promise.reject(error);
    },
);

// Bind Axios to Vue.
Vue.$http = Axios;
Object.defineProperty(Vue.prototype, '$http', {
  get() {
    return Axios;
  },
});
