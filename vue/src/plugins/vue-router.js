/* ============
 * Vue Router
 * ============
 *
 * The official Router for Vue.js. It deeply integrates with Vue.js core
 * to make building Single Page Applications with Vue.js a breeze.
 *
 * http://router.vuejs.org/en/index.html
 */

import Vue from 'vue';
import VueRouter from 'vue-router';

/**
 * Adds two numbers together.
 * @param {string} path Route path.
 * @param {string} view View name.
 * @param {string} name Route name.
 * @param {string} meta Metadata for the route.
 * @param {function} beforeEnter Route middleware.
 * @param {array} childs Route children.
 * @return {object} Route object definition.
 */
function vroute( {path, view, name, meta, beforeEnter, childs} ) {
  const preparedRoute = {
    name: name || view,
    path: path,
    component: (resolve) => import(
        `@/views/${view}.vue`
    ).then(resolve),
    meta: meta,
  };
  if ( typeof beforeEnter == 'function' ) {
    preparedRoute.beforeEnter = beforeEnter;
  }

  if(Array.isArray(childs) && childs.length>0) {
    preparedRoute.children = childs.map((route) => vroute(route));
  }

  return preparedRoute;
}

/**
 * The following block of code ill recursively scan this directory for Js
 * files and automatically register them with their "basename".
 * webpack => require.context(directory,useSubdirectories=false,regExp=/^\.\//);
 */

const routes = [];
const paths = require.context('@/routes', true, /\.js$/i);
paths.keys().map((key) => routes.push(...paths(key).default));

Vue.use(VueRouter);

export const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: routes.map((elmt) => vroute(elmt)).concat([
    {path: '/*', redirect: '/notfound'},
  ]),
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    }
    if (to.hash) {
      return {selector: to.hash};
    }
    return {x: 0, y: 0};
  },
});

Vue.router = router;

export default {
  router,
};
