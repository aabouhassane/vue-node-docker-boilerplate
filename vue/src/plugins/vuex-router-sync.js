/*
 * Leyton 4 Me
 * Copyright (c) 2020 Leyton Group - All Rights Reserved
 * contact@leyton.me
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

/* ============
 * Vuex Router Sync
 * ============
 *
 * Sync vue-router's current $route as part of vuex store's state.
 *
 * https://github.com/vuejs/vuex-router-sync
 */

import {sync} from 'vuex-router-sync';
import store from '@/store';
import {router} from './vue-router';

// Load router sync
sync(store, router);
