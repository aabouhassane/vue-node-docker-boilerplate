// vue.config.js

const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')

module.exports = {
    // options...
    devServer: {
        disableHostCheck: true
    },
    configureWebpack: {
        plugins: [
            new VuetifyLoaderPlugin()
        ],
    },
}
