# vue-node-docker-boilerplate

## Application Techs
##### Mariadb
##### VueJs/VueCli (Check README with vue folder)
##### Node/Express/Sequelize(DB) (Check README with node folder)

## Running the application

Premiérement il faut mettre un alias dns vers app.test:

Ajouter la ligne suivante dans votre fichier hosts

127.0.0.1   app.test

### In development
```
ln -s .env.development .env
./run-dev down [-v]
./run-dev up -d --build
```
### In production
```
ln -s .env.production .env
./run-prod down [-v]
./run-prod up -d --build
```

> The -v flag is optional, It is for removing the volumes when bringing down the application
