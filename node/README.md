# Node

## [Models/Sequelize](https://sequelize.org/master/manual/migrations.html):

### Generate Models
```
npx sequelize-cli model:generate --name Fred --attributes firstName:string,lastName:string,email:string
```

## Migration:

### Running Migration
```
npx sequelize-cli db:migrate
```

### Undoing Migrations

Revert most recent migration: 
```
npx sequelize-cli db:migrate:undo
```

Revert All migration: 
```
npx sequelize-cli db:migrate:undo:all
```

Revert specific migration: 
```
npx sequelize-cli db:migrate:undo:all --to XXXXXXXXXXXXXX-create-posts.js
```

## Seeds:

### Create Seeds
```
npx sequelize-cli seed:generate --name demo-user
```

### Running Seeds
```
npx sequelize-cli db:seed:all
```

### Undoing Seeds

Revert most recent seeds: 
```
npx sequelize-cli db:seed:undo
```

Revert specific seed: 
```
npx sequelize-cli db:seed:undo --seed name-of-seed-as-in-data
```

Revert all seeds: 
```
npx sequelize-cli db:seed:undo:all
```

## Inspecting Long Variables with "eyes":

```
const inspect = require('eyes')
            .inspector({
                    pretty: true,
                    hideFunctions: true,
                    maxLength: 32768
            });
inspect(MyLongVariable); => checkout console
```
