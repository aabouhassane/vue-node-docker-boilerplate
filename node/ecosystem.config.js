/*
 * The -i or instances option can be:
 *     0/max to spread the app across all CPUs
 *     -1 to spread the app across all CPUs-1
 *     number to spread the app across number CPUs
 */
module.exports = {
  apps : [
    {
      name: "myapp",
      script    : "app_bootstrap.js",
      instances : "4",
      exec_mode : "cluster",
      max_memory_restart: '100M',
      autorestart: true,
      env: {
        NODE_ENV: 'development'
      },
      env_production: {
        NODE_ENV: 'production'
      }
    }
  ]
}
