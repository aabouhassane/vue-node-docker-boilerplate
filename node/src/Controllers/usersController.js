const Status = require('http-status');

const usersController = ({userService}) => ({

  getAllUsers: async (req, res, next) => {
    try{
      console.log('req.query: ', req.query)
      const user = await userService.getAllUsers(req.query);
      res.status(200).json(user);
    }catch(err){
      next(err);
    }
  },

  addUser: async (req, res, next) => {
    try{
      console.log('req.body: ', req.body)
      const user = await userService.addUser(req.body);
      res.status(200).json(user);
    }catch(err){
      next(err);
    }
  },

  updateUser: async (req, res, next) => {
    try{
      console.log('req.body: ', req.body)
      const user = await userService.updateUser(req.body.userOldInfo, req.body.userUpdatedInfo);
      res.status(200).json(user);
    }catch(err){
      next(err);
    }
  },

  deleteUser: async (req, res, next) => {
    try{
      const wasDeleted = await userService.deleteUser(req.body.id);
      res.status(200).json(wasDeleted);
    }catch(err){
      next(err);
    }
  },

  getUser: async (req, res, next) => {
    try{
      console.log('req.query: ', req.query)
      const user = await userService.getUserInfo(1);
      res.status(200).json(user);
    }catch(err){
      next(err);
    }
  },

  getUserPosts: async (req, res, next) => {
    try{
      const userPosts = await userService.getUserPosts(1);
      res.status(200).json(userPosts);
    }catch(err){
      next(err);
    }
  }


});

module.exports = usersController;
