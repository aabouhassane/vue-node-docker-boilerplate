const Status = require('http-status');

const postsController = ({postService}) => ({

	getPostById: async (req, res) => {
		try{
			const post = await postService.getPost(1);
			res.status(200).json(post);
		}catch(err){
			next(err);
		}
	},

	getPostUser: async (req, res) => {
		try{
			const postUser = await postService.getPostUser(1);
			res.status(200).json(postUser);
		}catch(err){
			next(err);
		}
	}

})

module.exports = postsController;
