const DomainError = require('./domainError');

class UnauthorizedError extends DomainError {
    constructor(message, debugData={}, code=401) {
        super(message, debugData, code);
    }
}

module.exports = UnauthorizedError;
