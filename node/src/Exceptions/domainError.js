class DomainError extends Error {
    constructor(message, debugData={}, code=500) {
        super(message);
        // Ensure the name of this error is the same as the class name
        this.name = this.constructor.name;

        this.code = code;

        this.debugData = debugData;

        // capturing the stack trace keeps the reference to your error class
        Error.captureStackTrace(this, this.constructor);
    }

    statusCode() {
        return this.code;
    }
}

module.exports = DomainError;
