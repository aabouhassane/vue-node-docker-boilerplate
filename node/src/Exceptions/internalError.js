const DomainError = require('./domainError');

class InternalError extends DomainError {
    constructor(message, debugData={}, code=500) {
        super(message, debugData, code);
    }
}

module.exports = InternalError;
