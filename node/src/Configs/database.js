module.exports = {
  development: {
    database: process.env.DB_NAME,

    databaseUrl: !!(process.env.DB_URL && process.env.DB_URL!==''),

    // the sql dialect of the database
    // currently supported: 'mysql', 'sqlite', 'postgres', 'mssql', 'mariadb'
    dialect: process.env.DB_CONNECTION || 'mariadb',

    // custom host; default: localhost
    host: process.env.DB_HOST || 'localhost',
    port: process.env.DB_PORT || '3306',
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,

    // disable logging; default: console.log
    //logging: false,

    // disable inserting undefined values as NULL
    // - default: false
    omitNull: true,

    // Specify options, which are used when sequelize.define is called.
    // The following example: define: { timestamps: false }
    // is basically the same as: sequelize.define(name, attributes, { timestamps: false })
    // so defining the timestamps for each model will be not necessary
    define: {
      //schema: 'prefix',
      underscored: false,
      freezeTableName: true,
      charset: 'utf8', //utf8mb4
      dialectOptions: {
        collate: 'utf8_general_ci', //utf8mb4_unicode_ci
        bigNumberStrings: true
      },
      timestamps: true
    },

    // similar for sync: you can define this to always force sync for models
    // If force is true, each Model will run DROP TABLE IF EXISTS, before it tries to create its own table
    sync: { force: false },

    pool: {
      max: 5,
      min: 0,
      acquire: 60000,
      idle: 30000
    },
    operatorsAliases: {}
  },
  production: {
    database: process.env.DB_NAME,

    databaseUrl: !!(process.env.DB_URL && process.env.DB_URL!==''),

    // the sql dialect of the database
    // currently supported: 'mysql', 'sqlite', 'postgres', 'mssql'
    dialect: process.env.DB_CONNECTION || 'mariadb',

    // custom host; default: localhost
    host: process.env.DB_HOST || 'localhost',
    port: process.env.DB_PORT || '3306',
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,

    // disable logging; default: console.log
    logging: false,

    // disable inserting undefined values as NULL
    // - default: false
    omitNull: true,

    // Specify options, which are used when sequelize.define is called.
    // The following example: define: { timestamps: false }
    // is basically the same as: sequelize.define(name, attributes, { timestamps: false })
    // so defining the timestamps for each model will be not necessary
    define: {
      underscored: false,
      freezeTableName: false,
      charset: 'utf8', //utf8mb4
      dialectOptions: {
        collate: 'utf8_general_ci', //utf8mb4_unicode_ci
        bigNumberStrings: true
      },
      timestamps: false
    },

    // similar for sync: you can define this to always force sync for models
    // If force is true, each Model will run DROP TABLE IF EXISTS, before it tries to create its own table
    sync: { force: false },

    pool: {
      max: 5,
      min: 0,
      acquire: 60000,
      idle: 30000
    },
    operatorsAliases: {}
  }
};
