const path = require('path');
const logPath = path.join(__dirname, '../logs/development.log');

module.exports = {
    development: {
        appenders: {
            out: { type: 'console' },
            task: { type: 'file', filename: logPath }
        },
        categories: {
            default: { appenders: ['out', 'task'], level: 'info' }
        }
    },
    production: {
        appenders: {
            out: { type: 'console', layout: { type: 'basic' } },
            task: { type: 'file', filename: logPath }
        },
        categories: {
            default: { appenders: ['out', 'task'], level: 'info' }
        }
    },
}
