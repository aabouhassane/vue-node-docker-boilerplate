const {Op} = require('sequelize');
const path = require('path');

class postRepository {

  constructor({ PostModel }) {
    this.PostModel = PostModel;
  }

    async getPost(id) {
        try{
            return await this.PostModel.findOne({
                where: {
                    id: {
                        [Op.eq]: id
                    }
                }
            });
        } catch (err){
            throw err;
        }
    }

    async getPostUser(id) {
        try{
            return await this.PostModel.findOne({
                where: {
                    id: {
                        [Op.eq]: id
                    }
                },
                include: [
                    {
                        association: 'Owner',
                        required: false,
                    },
                ]
            });
        } catch (err){
            throw err;
        }
    }
}

module.exports = postRepository;
