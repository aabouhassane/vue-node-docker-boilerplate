const {Op} = require('sequelize');

class userRepository {

  constructor({ UserModel }) {
    this.UsersModel = UserModel;
  }

  async getAllUsers(queryParams) {
    try{
      let order = [],
          offset = queryParams.itemsPerPage * (queryParams.page-1);


      if(queryParams.sortBy && queryParams.sortBy.length>0){
        if(queryParams.sortDesc && queryParams.sortDesc.length>0 && queryParams.sortDesc[0] == 'true'){
          order.push([queryParams.sortBy[0], 'DESC'])
        } else {
          order.push([queryParams.sortBy[0], 'ASC'])
        }
      }

      return await this.UsersModel.findAndCountAll({
        // where: {...},
        order,
        limit: parseInt(queryParams.itemsPerPage),
        offset,
      });
    } catch (err){
      throw err;
    }
  }

  async addUser(userInfo) {
    try {
      console.log('userInfo: ', userInfo)
      return await this.UsersModel.create(userInfo).then(user => {
        console.log("New auto-generated ID:", user.id);
        return user;
      });
    } catch (err){
      throw err;
    }
  }

  async updateUser(oldData, newData) {
    try {
      return await this.UsersModel.update(
          newData,
          {
            where: {
              id: {
                [Op.eq]: oldData.id
              }
            }
          }
      );
    } catch (err){
      throw err;
    }
  }

  async deleteUser(userId) {
    try {
      return await this.UsersModel.destroy({
        where: {
          id: {
            [Op.eq]: userId
          }
        }
      });
    } catch (err){
      throw err;
    }
  }

  async getUserInfo(id) {
    try{
      return await this.UsersModel.findOne({
        where: {
          id: {
            [Op.eq]: id
          }
        }
      });
    } catch (err){
      throw err;
    }
  }

  async getUserPosts(id) {
    try{
      return await this.UsersModel.findOne({
        where: {
          id: {
            [Op.eq]: id
          }
        },
        include: [
          {
            association: 'Posts',
            required: false,
          },
        ]
      });
    } catch (err){
      throw err;
    }
  }
}

module.exports = userRepository;
