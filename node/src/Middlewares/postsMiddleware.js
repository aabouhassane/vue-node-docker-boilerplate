const postsMiddleware = {
	global: (req, res, next) => {
	  console.log('You Hit the Posts Route!!');
	  next();
	},

	getPost: (req, res, next) => {
		req.body.isValid = false;

		const { internalError } = req.container.cradle.errors;

    	if(req.body.isValid){
			console.log('You Hit the Post Route!!');
			next();
	    }else{
	    	// throw new error and cancel the request
	    	next(new internalError('Post is  not valid!!', {
	    		model: 'post',
				id: 1
			}));
	    }
	}
};

module.exports = postsMiddleware;
