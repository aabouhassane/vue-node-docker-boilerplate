const usersMiddleware = {
	global: (req, res, next) => {
		console.log('You Hit the Users Route!!');
		next();
	},
	getUser: (req, res, next) => {
		console.log('You Hit the /api/users/:id Route!!');
		next();
	}
};

module.exports = usersMiddleware;
