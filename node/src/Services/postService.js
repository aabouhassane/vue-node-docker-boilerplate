class postService {
    constructor({postRepository}) {
        this.postRepository = postRepository;
    }

    async getPost(id) {
        return this.postRepository.getPost(id);
    }

    async getPostUser(id) {
        return this.postRepository.getPostUser(id);
    }
}

module.exports = postService;
