class userService {
    constructor({userRepository}) {
        this.userRepository = userRepository;
    }

    async getAllUsers(queryParams) {
        return this.userRepository.getAllUsers(queryParams);
    }

    async addUser(userInfo) {
        return this.userRepository.addUser(userInfo);
    }

    async updateUser(oldData, newData) {
        return this.userRepository.updateUser(oldData, newData);
    }

    async deleteUser(userId) {
        return this.userRepository.deleteUser(userId);
    }

    async getUserInfo(id) {
        return this.userRepository.getUserInfo(id);
    }

    async getUserPosts(id) {
        return this.userRepository.getUserPosts(id);
    }
}

module.exports = userService;
