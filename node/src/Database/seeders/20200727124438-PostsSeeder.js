'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Posts', [
      {
        user_id: 1,
        title: 'My post',
        description: 'My post descritpion',
        category: 1,
        year: 1973,
        reference: '345',
        book_price: 765,
        published: false,
        createdAt: new Date(),
        updatedAt: new Date(),
      }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Posts', null, {});
  }
};
