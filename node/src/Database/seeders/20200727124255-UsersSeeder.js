'use strict';

module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.bulkInsert('Users', [
            {
                firstName: 'John',
                lastName: 'Doe',
                email: 'john.doe@gmail.com',
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                firstName: 'Mark',
                lastName: 'Green',
                email: 'mark.green@gmail.com',
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                firstName: 'Fred',
                lastName: 'Sami',
                email: 'fred.sami@gmail.com',
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                firstName: 'Brad',
                lastName: 'Andro',
                email: 'brad.andro@gmail.com',
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                firstName: 'Dream',
                lastName: 'Big',
                email: 'dream.big@gmail.com',
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                firstName: 'Crash',
                lastName: 'Bondico',
                email: 'crash.bondico@gmail.com',
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                firstName: 'Carla',
                lastName: 'Santiago',
                email: 'carla.santiago@gmail.com',
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                firstName: 'Deer',
                lastName: 'Mano',
                email: 'deer.mano@gmail.com',
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                firstName: 'Selshi',
                lastName: 'Marshal',
                email: 'selshi.marshal@gmail.com',
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                firstName: 'Arizona',
                lastName: 'Vinalo',
                email: 'arizona.vinalo@gmail.com',
                createdAt: new Date(),
                updatedAt: new Date(),
            },
        ], {});
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.bulkDelete('Users', null, {});
    }
};
