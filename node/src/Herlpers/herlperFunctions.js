module.exports = {
	stringifyMe: (obj) => {
	  switch (typeof(obj)) {
	      case "object":
	        return this.stringifyObject(obj);
	      case "function":
	        return '"[Function]"';
	      case "null":
	      case "undefined":
	        return "null";
	      case "string":
	        return obj.length>0? `"${this.stringifyString(obj)}"`:"null";
	      default:
	        return obj? `"${obj}"`:"null";
	  }
	},

	stringifyString: (str) => {
	  return str.replace(/\\/g, '\\\\')
	            .replace(/\n/g, '\\n')
	            .replace(/[\u0001-\u001F]/g, function (match) {
	                return '\\0' + match[0].charCodeAt(0).toString(8);
	            });
	},

	stringifyObject: (obj) => {
	    let out = [];
	    let that = this;
	    let keys = Object.getOwnPropertyNames(obj);
	    keys.forEach(function (k) {
	        if (Object.prototype.hasOwnProperty.call(obj, k)) {
	            out.push(`"${k}": ${that.stringifyMe(obj[k])}`);
	            //out.push(k + ': ' + that.stringifyMe(obj[k]));
	        }
	    });

	    if (out.length === 0) return '{}'
	    else return "{" + out.join(',') + "}";
	}
}
