'use strict';
const {
    Model,
    Op
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Post extends Model {

        static associate(models) {
            this.belongsTo(models.User, { as: 'Owner', foreignKey: 'user_id'});
        }

        // Instance Methods

    }

    Post.init({
        id: {
            type: DataTypes.INTEGER(11),
            primaryKey: true
        },
        title: DataTypes.STRING(25),
        description: DataTypes.TEXT('medium'),
        category: {
            type: DataTypes.TINYINT(4),
            get() {
                switch(this.getDataValue('type')){
                    case 1:
                        return 'Action';
                    case 2:
                        return 'Commedy';
                    case 3:
                        return 'Drama';
                    case 4:
                        return 'Educational';
                    default:
                        return 'Misc';
                }
            }
        },
        year: DataTypes.INTEGER(4),
        reference: DataTypes.ENUM('345', '765'),
        book_price: DataTypes.DECIMAL(20, 0),
        published: DataTypes.TINYINT(1),
    }, {
        sequelize,

        modelName: 'Post',

        tableName: 'Posts',

        getterMethods: {},

        setterMethods: {},

        defaultScope: {},

        scopes: {
            withCategory: (category) => {
                return {
                    where: {
                        category:{
                            [Op.eq]: category
                        }
                    },
                    order: sequelize.col('year')
                }
            },

            filteringData: () => {},

            filter: (filters) => {},
        }
    });

    return Post;
};
