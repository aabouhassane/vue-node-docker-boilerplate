'use strict';
const {
  Model
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class User extends Model {

    static associate(models) {
      this.hasMany(models.Post, { as: 'Posts', foreignKey: 'user_id'}); //will add a user_id to post
    }

    // Instance Methods

  }

  User.init({
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING
  }, {
    sequelize,

    modelName: 'User',

    tableName: 'Users',

  });

  return User;
};
