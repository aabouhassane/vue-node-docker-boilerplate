const { createController } = require('awilix-express');

// This Route Conroller
const postsController = require('../Controllers/postsController'),
	  postsMiddleware = require('../Middlewares/postsMiddleware');

const routehandler = createController(postsController)
						.prefix('/posts')  // Prefix all endpoints with `/posts`
						.before([postsMiddleware.global]) // run postsMiddleware for all endpoints
						.get('/:postId([0-9]{1,11})', 'getPostById', {
							before: [postsMiddleware.getPost]
						})
						.get('/user', 'getPostUser');

//when :postId is present in a route path, you may map 'postId' loading logic to automatically provide
//req.post to the route, or perform validations on the parameter input.
/*router.param('postId', function(req, res, next, id) {

  // try to get the post details from the Post model and attach it to the request object
  Post.find(id, function(err, user) {
    if (err) {
      next(err);
    } else if (post) {
      req.post = post;
      next();
    } else {
      next(new Error('failed to load the post'));
    }
  });
});
*/

module.exports = routehandler;
