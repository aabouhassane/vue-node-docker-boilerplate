const { createController } = require('awilix-express');


// This Route Conroller
const userController = require('../Controllers/usersController'),
	  userMiddleware = require('../Middlewares/usersMiddleware');

const routehandler = createController(userController)
						.prefix('/users')
						.before([userMiddleware.global])
						.get('/getAll', 'getAllUsers')
						.post('/add', 'addUser')
						.post('/delete', 'deleteUser')
						.post('/update', 'updateUser')
						.get('/get', 'getUser')
						.get('/getUserPosts', 'getUserPosts', {
							before: [userMiddleware.getUser]
						});


module.exports = routehandler;
