const path = require('path');

module.exports = function routeHandler(controllerUri) {
  const [controller, handler] = controllerUri.split('@');
  const controllerPath = path.resolve('server/src/Controllers', controller);
  return require(controllerPath)[handler];
};
