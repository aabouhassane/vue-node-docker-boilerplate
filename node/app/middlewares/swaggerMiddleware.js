const SwaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../../MyApp-Api-Documentation.json');

module.exports = [SwaggerUi.serve, SwaggerUi.setup(swaggerDocument)];
