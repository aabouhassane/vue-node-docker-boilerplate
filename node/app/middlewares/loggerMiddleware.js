const morgan = require('morgan'); // HTTP request logger middleware for node.js
const LoggerStreamAdapter = require('../logging/loggerStreamAdapter');

module.exports = ({ logger }) => {
  return morgan('dev', {
    stream: LoggerStreamAdapter.toStream(logger)
  });
};