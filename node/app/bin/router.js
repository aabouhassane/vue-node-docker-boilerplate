const { Router, static: serveStatic } = require('express');
const statusMonitor = require('express-status-monitor'),
      cors = require('cors'),
      bodyParser = require('body-parser'),
      compression = require('compression'),
      methodOverride = require('method-override'),
      { loadControllers } = require('awilix-express'),
      routesFolderPath = __dirname + '/../../src/';

module.exports = ({ config, containerMiddleware, loggerMiddleware, swaggerMiddleware, errorHandler }) => {
  const router = Router();

  if(config.env === 'development') {
    router.use(statusMonitor());
  }

  if(config.env !== 'test') {
    router.use(loggerMiddleware);
  }

  //router.use(containerMiddleware);

  const apiRouter = Router();

  if(process.env.APP_URL) {
    apiRouter.use(cors({
      origin: process.env.APP_URL
    }));
  }

  apiRouter
    .use(methodOverride('X-HTTP-Method-Override'))
    .use(bodyParser.json({limit: '50mb'})) // to support JSON-encoded bodies
    .use(bodyParser.urlencoded({ // to support URL-encoded bodies
      limit: '50mb',
      extended: true
    }))
    //.use(bodyParser.multipart());
    .use(compression())
    //.use(containerMiddleware)
    .use('/docs', swaggerMiddleware)
    .use(loadControllers('Routes/*.js', { cwd: routesFolderPath }));

  // prefix all apiRouter routes with /api
  router.use('/api', apiRouter);

  // dependent on the environment, the router will use a specific error handler
  router.use(errorHandler);

  return router;
};
