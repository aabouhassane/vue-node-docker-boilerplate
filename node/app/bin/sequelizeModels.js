const fs = require('fs'),
	  path = require('path'),
	  Sequelize = require('sequelize'),
	  ModelsDirPath = __dirname + '/../../src/Models',
	  { sequelizeConfigs } = require('../configsSetup'),
      Log4js = require('log4js'),
      logger = Log4js.getLogger(),
      chalk = require('chalk'),
      models = {};

let sequelize = {};
logger.level = 'info';

if(sequelizeConfigs) {
  const appDb = new Sequelize(sequelizeConfigs);


  fs.readdirSync(ModelsDirPath)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    // let model = MysqlDb.import(path.join(ModelsDirPath, file));
    let model = require(path.join(ModelsDirPath, file))(appDb, Sequelize.DataTypes);

    models[model.name] = model;
  });

  Object.keys(models).forEach(modelName => {
    if (models[modelName].associate) {
      models[modelName].associate(models);
    }
  });

  models.appDb = appDb;
  models.Sequelize = Sequelize;

  module.exports = models;
} else {
  logger.warn(chalk.yellow.bold('Sequelize Database configuration not found, disabling database.'));
}

/*if (databaseConfig.databaseUrl) {
  sequelize = new Sequelize(process.env.DATABASE_URL, databaseConfig);
} else {
  sequelize = new Sequelize(databaseConfig.database,
  							databaseConfig.username,
  							databaseConfig.password,
  							databaseConfig);
}*/



// dirname = C:\V12Projects\craiglist\server\Configs
// __filename = C:\V12Projects\craiglist\server\configs\database.js
// path.basename(__filename) = database.js
