const express = require('express'),
      helmet = require('helmet'),
      chalk = require("chalk");

class Server {
  constructor({ config, router, logger, containerMiddleware }) {
    this.config = config;
    this.logger = logger;
    this.router = router;
    this.containerMiddleware = containerMiddleware;
    this.express = express();

    // helmet disbales the "x-powered-by" header by default, in development mode
    // Yu'll still see the x-powered-by on the requests for the static files
    // As there serving is not handled by node
    //this.express.disable('x-powered-by');
    this.express.use(helmet()); // you can pass such a congig object {noCache: true}

    this.express.use(this.containerMiddleware);

    this.express.use(router);
  }

  start() {
    return new Promise((resolve) => {
      const http = this.express
        .listen(this.config.port, () => {
          const { port } = http.address();
          this.logger.info(chalk.redBright.bold(`[p ${process.pid}] Listening at port ${port}`));
          resolve();
        });
    });
  }
}

module.exports = Server;
