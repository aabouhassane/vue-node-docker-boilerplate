const fs = require('fs');
const path = require('path');

const configsBasePath = '../../src/Configs';

const ENV = process.env.APP_ENV || 'development';

const logConfig = require(path.join(__dirname, configsBasePath, 'logger.js'))[ENV];

const sqlzCfg = loadSequelizeConfigs();

const config = {
  [ENV]: true,
  env: ENV,
  port: process.env.APP_PORT,
  sequelizeConfigs: sqlzCfg,
  logging: logConfig
};

module.exports = config;


/**********************************************************************/
/***************************** Helpers ********************************/
/**********************************************************************/

function loadSequelizeConfigs() {
  if(process.env.DB_URL && process.env.DB_URL!=='') {
    return process.env.DB_URL;
  }

  const DatabaseConfigPath = path.join(__dirname, configsBasePath, 'database.js');

  if(fs.existsSync(DatabaseConfigPath)) {
    return require(DatabaseConfigPath)[ENV];
  }
}
