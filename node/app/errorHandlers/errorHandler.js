const Status = require('http-status');

/* Ignore next */
module.exports = (err, req, res, next) => {

  const { logger, config } = req.container.cradle;

  logger.error(err);

  if(err.code === undefined){
    res.status(Status.INTERNAL_SERVER_ERROR).json({
      type: 'InternalServerError',
      message: 'The server failed to handle this request'
    });
  }else{
    let errorData = {
      type: err.name,
      code: err.code,
      message: err.message
    };

    if(config.development) {
      errorData.debugData = err.debugData || 0;
      errorData.stack = err.stack || 0;
    }

    res.status(err.code).json(errorData);
  }
};
