const fs = require('fs'),
      path = require('path'),
      errorsDirPath = __dirname + '/../../src/Exceptions',
      exceptions = {};

fs.readdirSync(errorsDirPath)
    .filter(file => {
        return (file.indexOf('.') !== 0) && (file.slice(-3) === '.js');
    })
    .forEach(file => {
        let exception = require(path.join(errorsDirPath, file));

        exceptions[file.slice(0, -3)] = exception;
    });

module.exports = exceptions;
