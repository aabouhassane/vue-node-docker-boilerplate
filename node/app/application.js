const chalk = require('chalk');

class Application {
  constructor({ server, appDb, logger }) {
    this.server = server;
    this.appDb = appDb;
    this.logger = logger;

    if(appDb && appDb.options.logging) appDb.options.logging = logger.info.bind(logger);
  }

  async start() {
    if(this.appDb) {
      this.logger.info(chalk.magentaBright.bold('Connecting to Database...'));
      try{
        await this.appDb.authenticate();
        this.logger.info(chalk.magentaBright.bold('successufully connected to database!'));
      }catch(err){
        this.logger.error(chalk.red.bold(`Failed to connect to Database: ${err}`));
      }
    }

    await this.server.start();
  }
}

module.exports = Application;
