const { createContainer, asClass, asFunction, asValue, Lifetime } = require('awilix');
const { scopePerRequest } = require('awilix-express');

const config = require('./app/configsSetup/index');

const Application = require('./app/application');
const Server = require('./app/bin/server');
const router = require('./app/bin/router');

const errorHandler = require('./app/errorHandlers/errorHandler');

const loggerMiddleware = require('./app/middlewares/loggerMiddleware');
const swaggerMiddleware = require('./app/middlewares/swaggerMiddleware');

const logger = require('./app/logging/logger');

const userService = require('./src/Services/userService');
const postService = require('./src/Services/postService');

const userRepository = require('./src/Repositories/userRepository');
const postRepository = require('./src/Repositories/postRepository');

const { appDb,
        User: UserModel,
        Post: PostModel
      } = require('./app/bin/sequelizeModels');

const errors = require('./app/errorHandlers/errorLoader');

const container = createContainer();

// System
container
  .register({
    app: asClass(Application).singleton(),
    server: asClass(Server).singleton()
  })
  .register({
    router: asFunction(router).singleton(),
    logger: asFunction(logger).singleton()
  })
  .register({
    config: asValue(config),
    errors: asValue(errors),
  });

//Services
container.register({
    userService: asClass(userService).singleton(), //, { lifetime: Lifetime.SINGLETON }
    postService: asClass(postService).singleton()
});

// Middlewares
container
  .register({
    loggerMiddleware: asFunction(loggerMiddleware).singleton()
  })
  .register({
    containerMiddleware: asValue(scopePerRequest(container)),
    errorHandler: asValue(errorHandler),
    swaggerMiddleware: asValue([swaggerMiddleware])
  });

// Repositories
container.register({
  userRepository: asClass(userRepository).singleton(),
  postRepository: asClass(postRepository).singleton()
});

// Database Connections
container
  .register({
    appDb: asValue(appDb)
  });

// SequelizeDb Models
container
  .register({
    UserModel: asValue(UserModel),
    PostModel: asValue(PostModel)
  });

module.exports = container;
